Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  resources :users
  root'users#index'
  
  
  post 'top/login'
  get 'top/logout'
  get 'top/login'
  get 'top/main'
  
  resources :tweets
  post '/tweets/new',to:'tweets#new'
  
  resources :likes
  get 'likes/create'
end
