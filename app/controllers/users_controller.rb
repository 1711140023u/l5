class UsersController < ApplicationController
    def index
        @user = User.all
        @tweets = Tweet.all
    end
    
    def new
        @user = User.new
    end
    
    def create
        signup_pass = BCrypt::Password.create(params[:pass])
        @user = User.new(uid: params[:uid], pass: signup_pass)
        @user.save
        redirect_to '/'
    end
    
    def destroy
        user = User.find(params[:id])
        user.destroy
        redirect_to '/'
    end
    

end
