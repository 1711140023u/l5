class TopController < ApplicationController
    def main
        if session[:login_uid] == nil then
            render 'login'
        else
            @tweets = Tweet.all
            render 'main'
        end
        
    end
    
    def login
     if user = User.find_by_uid(params[:uid])
        uid = params[:uid]
        pass = params[:pass]
        if uid == user.uid && BCrypt::Password.new(user.pass) ==  pass then
            session[:login_uid] = user.uid
            
            redirect_to '/top/main'
        else
            render 'error'
        end
     end
    end

    def logout
        session.delete(:login_uid)
        redirect_to '/'
    end

end
