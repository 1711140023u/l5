class TweetsController < ApplicationController
    protect_from_forgery except: :search # searchアクションを除外
    def index
        @tweets = Tweet.all
    end
    def new
     @tweet = Tweet.new
    end
    def create
     user = User.find_by(uid: session[:login_uid])
     message= params[:tweet][:message]
     @tweet = Tweet.new(message: message,user_id:user.id)
     if @tweet.save
       flash[:notice] = "1 record added"
       redirect_to root_path
     else
       render 'new'
     end
    end

    def show
     @tweet = Tweet.find(params[:id])
    end
end
